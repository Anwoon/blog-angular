import {Component, Input, OnInit} from '@angular/core';
import {element} from 'protractor';
import {AdService} from '../services/ad.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(private adService: AdService) { }
  searchValue: string;

  ngOnInit() {
  }

  Search(search) {
    this.searchValue = search.value;
    this.adService.setFilter(search.value);
  }
}
