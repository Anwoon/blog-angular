import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AdService} from '../services/ad.service';

@Component({
  selector: 'app-view-one-post',
  templateUrl: './view-one-post.component.html',
  styleUrls: ['./view-one-post.component.scss']
})
export class ViewOnePostComponent implements OnInit {

  id: number;
  post: any;

  constructor(private route: ActivatedRoute, private adService: AdService, private router:Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.post = this.adService.getOnePost(this.id);
    if(this.post === undefined) {
      this.router.navigate(['posts']);
    }
  }

}
