export class Post {
   id: number;
   title: string;
   content: string;
   createdAt: Date;
   loveIts: number;


  constructor(id: number = 0, title: string = '', content: string = '', created_at: Date = new Date(), loveIts: number = 0) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.createdAt = created_at;
    this.loveIts = loveIts;
  }

  static create(o = {}) {
    const post = new Post();
    return Object.assign(post, o);
  }
}

