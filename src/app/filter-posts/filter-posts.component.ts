import { Component, OnInit } from '@angular/core';
import {AdService} from '../services/ad.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-filter-posts',
  templateUrl: './filter-posts.component.html',
  styleUrls: ['./filter-posts.component.scss']
})
export class FilterPostsComponent implements OnInit {

  postsFilter: any;
  constructor(private adService: AdService, private router: ActivatedRoute) { }

  ngOnInit() {
    this.postsFilter = this.adService.filterPosts(this.router.snapshot.params['label']);
    console.log(this.postsFilter);
  }

  filterASC() {
    this.adService.posts = this.adService.filterPosts('ASC');
  }

  filterDESC() {
    this.adService.posts = this.adService.filterPosts('DESC');
  }
}
