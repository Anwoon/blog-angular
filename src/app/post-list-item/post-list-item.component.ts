import {Component, Input, OnInit} from '@angular/core';
import {AdService} from '../services/ad.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  @Input() loveIts: number;
  @Input() createdAt: string;
  @Input() indexPost: number;

  constructor(private adService: AdService) { }

  ngOnInit() {
  }

  iLoveIt () {
    ++this.loveIts;
  }

  Delete() {
    this.adService.deletePost(this.indexPost);
  }

  iDontLoveIt () {
    --this.loveIts;
  }
}
