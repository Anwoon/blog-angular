import {Post} from '../models/post';
import {post} from 'selenium-webdriver/http';

export class AdService {

  posts: Post[] = [];
  filter: string = '';

  constructor() {
    for (let i = 0; i < 3; i++) {
      this.posts.push(new Post(i, 'Titre' + i,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet sollicitudin risus quis tristique.',
        new Date(2013,2,i,1,10),
        0));
    }
  }

  data() {
    return this.posts;
  }

  deletePost(id: number) {
    const index = this.posts.map(function(x){ return x.id; }).indexOf(id);
    this.posts.splice(index, 1);
  }

  refreshArray(array: any){
    this.posts = array;
    console.log(this.posts);
  }

  setFilter(filter:string) {
    this.filter = filter;
  }

  getFilteredResults() {
    return this.posts.filter(post => post.title.toLowerCase().includes(this.filter.toLowerCase()) );
  }

  addPost(form) {
    form.id = this.newId();
    console.log(form);
    this.posts.push(Post.create(form));
  }

  newId() {
    let newIdPost = 0;
    this.posts.forEach((element) => {
      newIdPost = Math.max(newIdPost, element.id);
    });
    return newIdPost + 1 ;
  }

  getOnePost(id) {
    return this.posts.find(function(element){
      return element.id === Number(id);
    });
  }

  filterPosts(label: string) {

    function SortTimeASC(a, b) {
      let da = new Date(a.createdAt);
      let db = new Date(b.createdAt);
      return (da > db) ? 1 : -1;
    }

    function SortTimeDESC(a, b) {
      let da = new Date(a.createdAt);
      let db = new Date(b.createdAt);
      return (da > db) ? -1 : 1;
    }

      if (label === 'ASC') {
        return this.posts.sort(SortTimeASC);
      }

      if (label === 'DESC') {
        return this.posts.sort(SortTimeDESC);
      }
    }
}
