import {Component, DoCheck, Input, OnInit} from '@angular/core';
import {Post} from '../models/post';
import {AdService} from '../services/ad.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, DoCheck {

  public posts: any[]

  constructor(private adService: AdService) {
  }

  ngDoCheck() {
    this.posts = this.adService.getFilteredResults();
  }

  ngOnInit() {
  }

}
