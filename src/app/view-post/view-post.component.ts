import { Component, OnInit } from '@angular/core';
import {AdService} from '../services/ad.service';
import {Post} from '../models/post';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.scss']
})
export class ViewPostComponent implements OnInit {
  title = 'app';
  posts: Post[] = [];

  constructor(private adService: AdService) {
  }

  ngOnInit() {
    this.posts = this.adService.data();
  }

}
