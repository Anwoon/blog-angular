import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostListItemComponent } from './post-list-item/post-list-item.component';
import { SearchComponent } from './search/search.component';
import { AdService } from './services/ad.service';
import {ViewPostComponent} from './view-post/view-post.component';
import {RouterModule, Routes} from '@angular/router';
import {EditComponent} from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ViewOnePostComponent} from './view-one-post/view-one-post.component';
import {FilterPostsComponent} from './filter-posts/filter-posts.component';

const appRoot: Routes = [
  { path: '', component: ViewPostComponent },
  { path: 'posts', component: ViewPostComponent},
  { path: 'new', component: EditComponent},
  { path: 'posts/:id', component: ViewOnePostComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    PostListItemComponent,
    SearchComponent,
    ViewPostComponent,
    EditComponent,
    ViewOnePostComponent,
    FilterPostsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoot),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AdService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
